BUILDID=$(shell date +%Y%m%d-%H:%M:%S)

all:
	make clean
	git commit -a -m 'Automatisk $(BUILDID)' || true
	git push
clean:
	cd src && doconce clean
	rm -rf src/Trash
	rm -rf out
	doconce clean
pdf:
	make clean
	cd src && doconce format pdflatex book --encoding=utf-8 --section_numbering=off --latex_style=Springer_sv --latex_title_layout=std
	cd src && ptex2tex book
	cd src && pdflatex book.tex
	cd src && pdflatex book.tex
	cd src && pdflatex book.tex
sphinx:
	make clean
	cd src && doconce format sphinx book --encoding=utf-8
	cd src && doconce split_rst book
	touch src/conf.py # WHY ???
	cd src && doconce sphinx_dir theme=sphinx_rtd_theme logo=../../logo_black.png version=0.1 book
	cd src && python2 automake_sphinx.py
	mkdir -p out
	cp -r src/sphinx-rootdir/_build/html/ out/sphinx
	# Index.html?
	# firefox out/sphinx/index.html
html:
	doconce format html src/book.do.txt --html_style=bootswatch_journal --no_title --encoding=utf-8
	mkdir -p out
	cp src/book.html out/book.html
	chromium out/book.html
FOLDERS=src/intro_python src/kursplan
slides_FOLDERS=$(addprefix slides_,$(FOLDERS))
.PHONY: force
$(slides_FOLDERS): force
	echo making slides_FOLDERS
	cd $(patsubst slides_%,%,$@) && doconce format html slides.do.txt --pygments_html_style=default --keep_pygments_html_bg --encoding=utf-8
	cd $(patsubst slides_%,%,$@) && doconce slides_html slides.html reveal --html_slide_theme=simple
	cd $(patsubst slides_%,%,$@) && doconce format pdflatex slides.do.txt --latex_title_layout=beamer --latex_admon_title_no_period --latex_code_style=pyg SLIDE_TYPE="beamer" SLIDE_THEME="blue_plain"
	cd $(patsubst slides_%,%,$@) && doconce slides_beamer slides --beamer_slide_theme=blue_plain
	cd $(patsubst slides_%,%,$@) && pdflatex -shell-escape slides.tex
	cd $(patsubst slides_%,%,$@) && pdfnup --nup 2x3 --frame true --delta "1cm 1cm" --scale 0.9 --outfile slides-handout.pdf slides.pdf
	mkdir -p out/slides/
	mv $(patsubst slides_%,%,$@)/slides.pdf out/slides/$(shell basename $(patsubst clean_%,%,$@)).pdf
	mv $(patsubst slides_%,%,$@)/slides-handout.pdf out/slides/$(shell basename $(patsubst clean_%,%,$@))-handout.pdf
	mv $(patsubst slides_%,%,$@)/slides.html out/slides/$(shell basename $(patsubst clean_%,%,$@)).html
	cd $(patsubst slides_%,%,$@) && doconce clean
	chromium out/slides/$(shell basename $(patsubst clean_%,%,$@)).html &
slides: $(slides_FOLDERS)
	echo making slides
	#rm -rf out/slides/reveal.js out/slides/deck.js
	mkdir -p out/slides
	mv src/kursplan/reveal.js out/slides
